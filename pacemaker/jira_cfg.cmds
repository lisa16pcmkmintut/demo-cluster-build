# BEFORE YOU add the jira resources, you need
#   the db and haproxy resources
#

# Get a working copy of the CIB to update
pcs cluster cib temp-cib.xml

# Create the JIRA resource.
#
APPDIR=/opt/atlassian/jira
pcs -f temp-cib.xml resource create Jira_JVM ocf:heartbeat:tomcat \
  params \
    java_home=/usr/java/latest \
    catalina_home=${APPDIR} \
    tomcat_name=jiraTomcat \
    tomcat_user=jira \
    statusurl=http://localhost:8080 \
    catalina_pid=${APPDIR}/work/catalina.pid \
    tomcat_start_script=${APPDIR}/bin/catalina.sh \
    catalina_tmpdir=${APPDIR}/temp \
  op start   interval="0s"  timeout="300s" on-fail="restart" \
  op stop    interval="0s"  timeout="180s" on-fail="block"   \
  op monitor interval="15s" timeout="60s"  on-fail="restart" \
  --force

## Jira_JVM *may* run where on nodes with jiraServer=true
# Without this, the group can't be anywhere
pcs -f temp-cib.xml constraint location Jira_JVM \
  rule resource-discovery=exclusive \
  score="INFINITY" jiraServer eq 'true'

# IP address that the JIRA users will connect with 
pcs -f temp-cib.xml resource create Jira_IP ocf:heartbeat:IPaddr2 \
  ip=192.168.56.251 \
  op start   interval="0s"  timeout="20s"  on-fail="restart"  \
  op stop    interval="0s"  timeout="20s"  on-fail="block"    \
  op monitor interval="30s" timeout="20s"  on-fail="restart"

## Jira_IP *may* run where on nodes with haproxyServer=true
# Without this, the group can't be anywhere
pcs -f temp-cib.xml constraint location Jira_IP
  rule resource-discovery=exclusive \
  score="INFINITY" haproxyServer eq 'true'

# Jira_IP must be colocated with and HAProxy clone
# this ensures the IP is on node with a running HAProxy clone
pcs -f temp-cib.xml constraint colocation add \
    Jira_IP with HAProxy_Service-clone INFINITY

# Now that the config is loaded into the staging cib, tell
# Pacemaker to apply the changes to the running config.
pcs cluster cib-push temp-cib.xml

# And clean up:
#
rm temp-cib.xml
