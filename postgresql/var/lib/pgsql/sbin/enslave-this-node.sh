#!/bin/bash

#------------------------------------------------------------------------
# 2016-03-23 mike.diehn@cd-adapco.com
#
# Based on the start_replication.sh script I wrote for the pgsql
# replication slaves for jira, confluence, etc., database DR.
#
# This script eases the process of preparing the non-Master
# pgsql-ha0X server to be started by Pacemaker as the slave. You
# should find it in the postgres user's $HOME/sbin on any server
# that might need to assume this role
#
# You'll use it if the HA cluster fails over the database server
# because it will leave the old master intentially "stuck down"
# so it can't accidentally startup as the slave with old data.
#
# Process:
#   1. use pg_basebackup to copy the current state of the master
#      here to the secondary. Once that's done, this secondary
#      *can* be started to replicate. But don't do that!
#   2. remove the ~postgres/tmp/PGSQL.lock file with which
#      Pacemaker "stuck" this server down.
#   3. Tell the user the command to issue to pacemaker to get it
#      to start the service here so they can cut & paste it.
#
# NOTE: You'll need ~postgres/.pgpass containing roughly:
#
#   # Line format:
#   #   hostname:port:database:username:password 
#   # You can use '*' as a wildcard anywhere except the password field.
#   # Match the hostname to the one you use in your commands.
#   #
#   *:5432:*:replicator:thepassword
#
#------------------------------------------------------------------------

      pg_ver=9.4
      pg_bin=/usr/pgsql-$pg_ver/bin
  pg_service=postgresql-$pg_ver
 pg_data_dir=${PGDATA:=/var/lib/pgsql/$pg_ver/data}
pg_repl_user=replicator
pg_repl_pass=SnoozeBuggy6

# user should pass in the master's hostname but if she
#  doesn't, use this default:
  pg_primary=${1:-pgsql-ha-rep}

       PGCTL="${pg_bin}/pg_ctl"
      PGBKUP="${pg_bin}/pg_basebackup"

if [ $(id -un) != "postgres" ]; then
  echo Run me only as user "postgres", please
  exit 1
fi

if ${PGCTL} status &>/dev/null
then
    echo "================================================================="
    echo "WHOA NELLY! There's a server running here! See:"
    echo "-----------------------------------------------------------------"
    ${PGCTL} status | sed 's/^/  /'
    echo "-----------------------------------------------------------------"
    echo "I'm not running pg_basebackup until you take care of that"
    echo "================================================================="
    exit 1
fi

echo Cleaning up old backups of pgsql data directories
rm -vrf ${pg_data_dir},.as-was.*
echo Move the data directories out of the way
mv -v ${pg_data_dir}{,.as-was.$(date +%F-%T)}

# No need to use the -R option here, Pacemaker's pgsql RA creates
# and appropriate one. It'll also remove old ones if it finds
# them.
echo Starting base backup as replicator
${PGBKUP} -h $pg_primary -D $pg_data_dir -U $pg_repl_user -v -P

cat <<'EOM'



=================================================================
Base backup is done, as you can see. The next steps are up
to you, my human friend.



To get Pacemaker to re-consider starting the pgsql slave,
run the following commands as *root*:

Again, do these as root!

# First, remove the lock that is preventing Pacemaker starting
# pgsql here
#
rm -f -v ~postgres/tmp/PGSQL.lock

# Then, tell Pacemaker to clear the records of the status of
# the named resource. When it does, it will go check the status
# again and try to put things into the ideal state.
#
pcs resource cleanup PgSQL_Service

EOM
