[ -f /etc/profile ] && source /etc/profile
[ -f $HOME/.bashrc ] && source $HOME/.bashrc
# If you want to customize your settings,
# Use the file below. This is not overridden
# by the RPMS.
[ -f $HOME/.pgsql_profile ] && source $HOME/.pgsql_profile
