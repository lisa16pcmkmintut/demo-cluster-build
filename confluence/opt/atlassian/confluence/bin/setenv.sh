#!/bin/bash

# Confluence home directory
#
APP_HOME=/var/atlassian/confluence

# Use a modern java version
#
JAVA_HOME=/usr/java/latest

# Minimum Tomcat heap space (for performance reasons on high load,
# this value should be the same as MAX_HEAP)
#
#MIN_HEAP=6144m

# Maximum Tomcat heap space
#
#MAX_HEAP=6144m

# This should be roughly 40% of your MAX_HEAP
#
#EDEN_SIZE=2467m

# Default PermGen size is still 64M in java 1.7. Ugh.
#  Monitoring of our production Confluence shows that
#  we need at least 175m.  So let's set this to a nice
#  round number well above that:
#
# 2015-05-22 mdiehn: hit the ceiling, so I doubled it for now.
#
#PERM_SIZE=512m

# Put the memory options together here for legibility later
#
#MEM_OPTS="-Xms$MIN_HEAP -Xmx$MAX_HEAP -XX:NewSize=$EDEN_SIZE -XX:MaxPermSize=$PERM_SIZE"
# Let java 8 size itself.
#MEM_OPTS="-XX:NewSize=$EDEN_SIZE -XX:MaxPermSize=$PERM_SIZE"

# Additional options
# see http://confluence.atlassian.com/display/DOC/Recognised+System+Properties for a complete list of options
#
#EXTRA_OPTS=""

# Java Monitoring Extension options
#
JMX_OPTS="-Dcom.sun.management.jmxremote.port=9996 -Dcom.sun.management.jmxremote.ssl=false -Dcom.sun.management.jmxremote.authenticate=false"

# Network family options
#
NET_OPTS="-Djava.net.preferIPv4Stack=true -Djava.net.preferIPv4Addresses"

## TUNING OPTIONS ----------------------------------------------------------------
#
# Most of these optimizations come from
# http://java.sun.com/performance/reference/whitepapers/tuning.html
# http://confluence.atlassian.com/display/DOC/Garbage+Collector+Performance+Issues
# http://blog.sokolenko.me/2014/11/javavm-options-production.html (mdiehn 2015-0702)

# No Tuning per default
#TUN_EXTRA_OPTS=""

# Make a server a server
#  Though this option is implicitely enabled for x64 virtual machines, it still makes sense
#  to use it as according to documentation behaviour maybe changed in the future.
#
TUN_EXTRA_OPTS="-server"

# This setting enables the latest performance optimizations from the Java team
# Be cautious, as this option enables/disables other options and is subject to changes
# from release to release
# In general, this has proven to give Confluence a decent performance boost
#
TUN_EXTRA_OPTS="$TUN_EXTRA_OPTS -XX:+AggressiveOpts"

# Enable GC optimizations
#   2015-0702 mdiehn from http://blog.sokolenko.me/2014/11/javavm-options-production.html
#
#TUN_EXTRA_OPTS="$TUN_EXTRA_OPTS -XX:+UseConcMarkSweepGC"
# 2015-10-14 mdiehn removing tuning to see what it does on it's own for a while.
#
##TUN_EXTRA_OPTS="$TUN_EXTRA_OPTS -XX:+CMSParallelRemarkEnabled"
##TUN_EXTRA_OPTS="$TUN_EXTRA_OPTS -XX:+UseCMSInitiatingOccupancyOnly"
##TUN_EXTRA_OPTS="$TUN_EXTRA_OPTS -XX:CMSInitiatingOccupancyFraction=70"
##TUN_EXTRA_OPTS="$TUN_EXTRA_OPTS -XX:+ScavengeBeforeFullGC"
##TUN_EXTRA_OPTS="$TUN_EXTRA_OPTS -XX:+CMSScavengeBeforeRemark"
TUN_EXTRA_OPTS="$TUN_EXTRA_OPTS -XX:+PrintGCDetails -XX:+PrintGCDateStamps -XX:+PrintHeapAtGC"
TUN_EXTRA_OPTS="$TUN_EXTRA_OPTS -Xloggc:$APP_HOME/logs/confluence-jvm-gc.started_$(date +%F_%H%M%S).log"
TUN_EXTRA_OPTS="$TUN_EXTRA_OPTS -XX:+UseGCLogFileRotation -XX:NumberOfGCLogFiles=20 -XX:GCLogFileSize=100M"


# Get the performance from 32-bit JVM, but allow 64-Bit registers
# This feature is enabled by default since JDK 6 Update 21
#
#TUN_EXTRA_OPTS="$TUN_EXTRA_OPTS -XX:+UseCompressedOops"

# Enable compiler escape analysis
# See http://en.wikipedia.org/wiki/Escape_analysis for what this means
# This feature is enabled by default since JDK 6 Update 21
#
#TUN_EXTRA_OPTS="$TUN_EXTRA_OPTS -XX:+DoEscapeAnalysis"

# If JDK 6 Update 20 or later is installed, enable this
#
TUN_EXTRA_OPTS="$TUN_EXTRA_OPTS -XX:+OptimizeStringConcat"

# If you run on an Intel CPU with 'Quick Path Interconnect', then enable this
#
TUN_EXTRA_OPTS="$TUN_EXTRA_OPTS -XX:+UseNUMA"

# Number of seconds during which DNS record will be cached in Java VM
#  60 seconds is a good value, default behaviors when this isn't set 
#  vary with version too much to leave this unset.
#
TUN_EXTRA_OPTS="$TUN_EXTRA_OPTS -Dsun.net.inetaddr.ttl=60"

# Dump the heap if we run out of memory so we don't loose the info
#  and have to provoke it again to analyze the cause.
#
TUN_EXTRA_OPTS="$TUN_EXTRA_OPTS -XX:+HeapDumpOnOutOfMemoryError -XX:HeapDumpPath=$APP_HOME/logs/confluence-oom_heapdump.`date +%F-%T`.hprof"

## END OF TUNING OPTIONS ---------------------------------------------------------


# Write a PID file
export CATALINA_PID=/opt/atlassian/confluence/work/catalina.pid

# These are our Java parameters for all JVMs we start
#
export JAVA_OPTS="-server -Djava.awt.headless=true -Dconfluence.home=$APP_HOME"

# Add these parameters to the start, run and debug JVMs we start
#
export CATALINA_OPTS="$MEM_OPTS $EXTRA_OPTS $TUN_EXTRA_OPTS $JMX_OPTS $NET_OPTS"

