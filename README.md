# README #

### What is this repository for? ###

The files and directories here can be used to create a Pacemaker HA cluster on CentOS 7.* for Atlassian Confluence and Atlassian JIRA using PostgreSQL and HAProxy.

### How do I get set up? ###

Use the files here in this order:

Prepare the nodes generically

* os.notes
* pacemaker.notes

Create the cluster configuration

* cluster_cfg.cmds
* postgresql_cfg.cmds
* haproxy_cfg.cmds
* jira_cfg.cmds
* confluence_cfg.cmds

Create the database

* postgresql.notes

Create the applications

* jira.notes
* confluence.notes
* haproxy.notes

### Who do I talk to? ###

Mike Diehn